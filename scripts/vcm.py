#!/usr/bin/env python3
import sys
import requests
from datetime import datetime, timedelta
import json
import hvac
import psycopg2
import os


def Merge(dict1, dict2):
    res = {**dict1, **dict2}
    return res


def graphite_date(original_date):
    return original_date.strftime("%H:%M_%Y%m%d")


def iter_times(increment='days'):
    now = datetime.now()
    earliest_time = now - timedelta(days=60)
    current_time = earliest_time
    time_ranges = []

    while current_time <= now:
        time = current_time
        if increment == 'days':
            start = datetime(time.year, time.month, time.day, 0, 0)
            end = datetime(time.year, time.month, time.day, 23, 59)
            time_ranges.append([start, end])
            current_time += timedelta(days=1)
        elif increment == 'hours':
            start = datetime(time.year, time.month, time.day, time.hour, 0)
            end = datetime(time.year, time.month, time.day, time.hour, 59)
            time_ranges.append([start, end])
            current_time += timedelta(hours=1)
        elif increment == 'minutes':
            start = datetime(time.year, time.month, time.day, time.hour,
                             time.minute, 0)
            end = datetime(time.year, time.month, time.day, time.hour,
                           time.minute, 59)
            time_ranges.append([start, end])
            current_time += timedelta(minutes=1)
    return time_ranges


def main():

    base = "vcm.vms"
    base_metric_name = "vcm_total"

    global_labels = {'instance': 'vcm.duke.edu:443', 'job': 'bulk_import'}

    metric_labels_lookup = {
        'reserved_rhel_7': {
            'os': 'LinuxVm',
            'status': 'reserved',
            'app': 'Generic Rhel7'
        }
    }

    server = 'timescale-test-01.oit.duke.edu'
    vault = hvac.Client(url=os.environ['VAULT_ADDR'],
                        token=os.environ['VAULT_TOKEN'])

    creds = vault.read('secret/linux/timescaledb/%s/creds/postgres' %
                       server)['data']

    conn = psycopg2.connect(
        "host=%s user=%s password=%s sslmode=require dbname=perfdump" %
        (server, creds['username'], creds['password']))
    cur = conn.cursor()

    metric_targets = []
    for metric_name_raw in requests.get(
            "https://graphite.oit.duke.edu/metrics/find?query=%s.*" %
            base).json():
        metric_targets.append(metric_name_raw['text'])

    for metric_target in metric_targets:

        metric_labels = metric_labels_lookup.get(metric_target)

        # Skip if we don't know what it is
        if not metric_labels:
            print("Don't know what to do with: %s" % metric_target)
            return 2

        all_metric_labels = Merge(global_labels, metric_labels)

        metrics_json = json.dumps(all_metric_labels)

        # Do we have this metric yet?
        cur.execute(
            "SELECT * from metrics_labels WHERE metric_name = '%s' and labels = '%s'"
            % (base_metric_name, metrics_json))
        existing_labels = cur.fetchall()
        if len(existing_labels) == 0:
            print("Creating labels for %s" % metrics_json)
            cur.execute(
                "INSERT INTO metrics_labels (metric_name, labels) VALUES(%s, %s)",
                (base_metric_name, metrics_json))
            conn.commit()

            cur.execute(
                "SELECT * from metrics_labels WHERE metric_name = '%s' and labels = '%s'"
                % (base_metric_name, metrics_json))
            labels_id = cur.fetchone()[0]
        else:
            labels_id = existing_labels[0][0]

        print(labels_id)

        for start, end in iter_times(increment='days'):
            print(start, end)

            for value, timestamp in requests.get(
                    'https://graphite.oit.duke.edu/render?target=%s.%s&format=json&from=%s&until=%s'
                    % (base, metric_target, graphite_date(start),
                       graphite_date(end))).json()[0]['datapoints']:
                if not value:
                    continue

                timestamp_o = datetime.fromtimestamp(timestamp)
                print(metric_target, all_metric_labels, value, timestamp_o)

                cur.execute(
                    "INSERT INTO metrics_values (time, value, labels_id) VALUES(%s, %s, %s) ON CONFLICT DO NOTHING",
                    (timestamp_o, value, labels_id))
            conn.commit()

    return 0


if __name__ == "__main__":
    sys.exit(main())
